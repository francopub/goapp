package main

import (
	"fmt"
	"log"
	"os/exec"
	"runtime"
)

func main() {
	fmt.Println("Hello, world!")
	cmd := exec.Command("settings", "put", "secure", "location_providers_allowed", "+gps")
	//cmd := exec.Command("bash", "-c",`"settings put secure location_providers_allowed +gps"`)
	
	if runtime.GOOS == "windows" {
		cmd = exec.Command("tasklist")
	}
	err := cmd.Run()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}
}
